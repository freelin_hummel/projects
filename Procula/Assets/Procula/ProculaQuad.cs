﻿using UnityEngine;
using System.Collections;

public class ProculaQuad : MonoBehaviour {
	public int m_Width;
	public int m_Length;
	public int m_GridSize;
	public float m_Height;
	public float m_RoofHeight;
	public float m_RoofOverhang;
	public Vector3 m_StartPosition;

	// Use this for initialization
	void Start () {
		MeshFilter filter = GetComponent<MeshFilter>();
		Procula procula = new Procula(filter);


		//procula.BuildSimpleGrid(m_Width, m_Length, m_GridSize, m_Height);
		//procula.BuildConnectedGrid(m_StartPosition, m_Width, m_Length, m_GridSize, m_Height);
		//procula.BuildCube(m_Height, m_Width, m_Length, Procula.Pivot.NearCorner);

		//procula.CreateConnectedGrid(m_StartPosition, m_Width, m_Length, m_GridSize, m_Height);
		//procula.CreateCube(m_Height, m_Width, m_Length, Procula.Pivot.Center);
		procula.CreateHouse(m_StartPosition, m_Height, m_Width, m_Length, m_RoofHeight, m_RoofOverhang);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
