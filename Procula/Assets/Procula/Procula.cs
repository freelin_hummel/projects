﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Procula
{
	#region Procula Variables

	MeshData _meshData;
	public List<Vector3> Normals { get { return _meshData.Normals; } }
	public List<Vector3> Vertices { get { return _meshData.Vertices; } }
	public List<Vector2> UVs { get { return _meshData.UVs; } }
	public List<int> Indices { get { return _meshData.Indices; } }

	private MeshFilter _meshFilter = null;
	
	#endregion
	#region Enumerations / Utility vars
	public Vector2 UV0 = new Vector2(0.0f, 0.0f);
	public Vector2 UV1 = new Vector2(0.0f, 1.0f);
	public Vector2 UV2 = new Vector2(1.0f, 1.0f);
	public Vector2 UV3 = new Vector2(1.0f, 0.0f);

	public enum Pivot
	{
		NearCorner,
		Center,
		CenterBottom
	}
	#endregion
	#region Constructors
	public Procula()
	{
		//Empty
		_meshData = new MeshData();
	}

	public Procula(MeshFilter meshFilter)
	{
		_meshData = new MeshData();
		_meshFilter = meshFilter;
	}
	#endregion

	#region Mesh Building Methods
	public void BuildTriangle(Vector3 v0, Vector3 v1, Vector3 v2)
	{
		Vector3 normal = Vector3.Cross((v1 - v0), (v2 - v0)).normalized;

		AddVert(v0, UV0, normal);
		AddVert(v1, UV1, normal);
		AddVert(v2, UV2, normal);
		AddTriangle();
	}

	private void BuildTrianglesForQuad(int vertsPerRow)
	{
		int index = _meshData.Vertices.Count - 1;

		int index0 = index;
		int index1 = index - 1;
		int index2 = index - vertsPerRow;
		int index3 = index - vertsPerRow - 1;

		AddTriangleIndices(index0, index2, index1);
		AddTriangleIndices(index2, index3, index1);
	}

	private void BuildQuad(int width, int length, Vector3 position)
	{
		//Set up the vertices and triangles:
		AddVert(
			new Vector3(0.0f, 0.0f, 0.0f) + position,
			UV0,
			Vector3.up
		);

		AddVert(
			new Vector3(0.0f, 0.0f, length) + position,
			UV1,
			Vector3.up
		);

		AddVert(
			new Vector3(width, 0.0f, length) + position,
			UV2,
			Vector3.up
		);

		AddVert(
			new Vector3(width, 0.0f, 0.0f) + position,
			UV3,
			Vector3.up
		);

		AddTrianglesForQuad();
	}


	void BuildQuad(Vector3 position, Vector3 widthDir, Vector3 lengthDir)
	{
		Vector3 normal = Vector3.Cross(lengthDir, widthDir).normalized;

		AddVert(position,
			UV0, normal);

		AddVert(position + lengthDir,
			UV1, normal);

		AddVert(position + lengthDir + widthDir,
			UV2, normal);

		AddVert(position + widthDir,
			UV3, normal);

		AddTrianglesForQuad();
	}

	void BuildDoubleSidedQuad(Vector3 position, Vector3 widthDir, Vector3 lengthDir)
	{
		BuildQuad(position, widthDir, lengthDir);
		BuildQuad(position, lengthDir, widthDir);
	}

	private void BuildQuadForGrid(Vector3 position, Vector2 uv, bool buildTriangles, int vertsPerRow)
	{
		Vertices.Add(position);
		UVs.Add(uv);

		if (buildTriangles)
		{
			BuildTrianglesForQuad(vertsPerRow);
		}
	}
	
	public void BuildSimpleGrid(int width, int length, int gridSize, float maxHeight)
	{
		for (int xGridPos = 0; xGridPos < gridSize; xGridPos++)
		{
			float z = length * xGridPos;

			for (int zGridPos = 0; zGridPos < gridSize; zGridPos++)
			{
				float x = width * zGridPos;

				float height = Random.Range(0.0f, maxHeight);
				Vector3 position = new Vector3(x, height, z);

				BuildQuad(width, length, position);
			}
		}
	}

	public void BuildConnectedGrid(Vector3 startPosition, int width, int length, int gridSize, float maxHeight)
	{
		for (int xGridPos = 0; xGridPos <= gridSize; xGridPos++)
		{
			float z = length * xGridPos;
			float v = (1.0f / gridSize) * xGridPos;

			for (int zGridPos = 0; zGridPos <= gridSize; zGridPos++)
			{
				float x = width * zGridPos;
				float u = (1.0f / gridSize) * zGridPos;

				float height = Random.Range(0.0f, maxHeight);
				Vector3 position = new Vector3(x, height, z);
				position += startPosition;

				Vector2 uv = new Vector2(u, v);
				bool buildTriangles = xGridPos > 0 && zGridPos > 0;

				BuildQuadForGrid(position, uv, buildTriangles, gridSize + 1);
			}
		}
	}
	#endregion

	#region Creators

	public void CreateSimpleGrid(int width, int length, int gridSize, float maxHeight)
	{
		BuildSimpleGrid(width, length, gridSize, maxHeight);
		CreateMesh();
	}

	public void CreateConnectedGrid(Vector3 startPosition, int width, int length, int gridSize, float maxHeight)
	{
		BuildConnectedGrid(startPosition, width, length, gridSize, maxHeight);
		CreateMesh();
	}

	public void CreateHouse(Vector3 position, float height, float width, float length, float roofHeight, float roofOverhang)
	{
		HouseBuilder house = new HouseBuilder(_meshData, position, height, width, length, roofHeight, roofOverhang);
		house.Build();
		CreateMesh();
	}

	public void CreateCube(Vector3 position, float height, float width, float length, Pivot pivot, MeshFilter meshFilter)
	{
		CubeBuilder cube = new CubeBuilder(_meshData, position, height, width, length, pivot);
		cube.Build();
		CreateMesh();
	}
	#endregion

	#region MeshData Data
	public class MeshData
	{
		private List<Vector3> m_Vertices = new List<Vector3>();
		private List<Vector3> m_Normals = new List<Vector3>();
		private List<Vector2> m_UVs = new List<Vector2>();
		private List<int> m_Indices = new List<int>();

		public List<Vector3> Normals { get { return m_Normals; } }
		public List<Vector3> Vertices { get { return m_Vertices; } }
		public List<Vector2> UVs { get { return m_UVs; } }
		public List<int> Indices { get { return m_Indices; } }
	}

	#endregion

	#region Adders
	public void AddTriangleIndices(int index0, int index1, int index2)
	{
		Indices.Add(index0);
		Indices.Add(index1);
		Indices.Add(index2);
	}

	public void AddVert(Vector3 position, Vector2 uv, Vector3 normal)
	{
		Vertices.Add(position);
		UVs.Add(uv);
		Normals.Add(normal);
	}

	public void AddTrianglesForQuad()
	{
		int index = Vertices.Count - 4;

		AddTriangleIndices(index, index + 1, index + 2);
		AddTriangleIndices(index, index + 2, index + 3);
	}

	public void AddTriangle()
	{
		int index = Vertices.Count - 3;

		AddTriangleIndices(index, index + 1, index + 2);
	}

	public void CreateMesh()
	{
		Mesh mesh = new Mesh();

		mesh.vertices = Vertices.ToArray();
		mesh.triangles = Indices.ToArray();

		//Normals are optional. Only use them if we have the correct amount:
		if (Normals.Count == Vertices.Count)
			mesh.normals = Normals.ToArray();

		//UVs are optional. Only use them if we have the correct amount:
		if (UVs.Count == Vertices.Count)
			mesh.uv = UVs.ToArray();

		mesh.RecalculateBounds();
		mesh.RecalculateNormals();

		if (_meshFilter != null)
		{
			_meshFilter.sharedMesh = mesh;
		}
	}
	#endregion
	#region Builders

	#region Cube
	class CubeBuilder : Procula
	{
		#region Cube Variables
		public Vector3 upDir;
		public Vector3 rightDir;
		public Vector3 forwardDir;
		public Vector3 farCorner;
		public Vector3 nearCorner;
		public Vector3 pivotOffset;
		public Vector3 position;
		#endregion

		#region Initialize Cube
		public CubeBuilder(MeshData meshData, Vector3 position, float height, float width, float length, Pivot pivot)
		{
			#region Non-Calculated Variables:
			_meshData = meshData;
			this.position = position;
			#endregion

			#region Calculated Variables:
			upDir = Vector3.up * height;
			rightDir = Vector3.right * width;
			forwardDir = Vector3.forward * length;

			farCorner = upDir + rightDir + forwardDir;
			nearCorner = Vector3.zero;

			pivotOffset += position;
			if (pivot == Pivot.Center)
			{
				farCorner /= 2;
				nearCorner = -farCorner;
			}
			else if (pivot == Pivot.CenterBottom)
			{
				pivotOffset = (rightDir + forwardDir) * 0.5f;
				farCorner -= pivotOffset;
				nearCorner -= pivotOffset;
			}
			#endregion
		}
		#endregion

		#region Build Faces
		protected void BuildFaceDown()
		{
			BuildQuad(nearCorner, forwardDir, rightDir);
		}

		protected void BuildFaceFront()
		{
			BuildQuad(nearCorner, rightDir, upDir);
		}

		protected void BuildFaceLeft()
		{
			BuildQuad(nearCorner, upDir, forwardDir);
		}

		protected void BuildFaceUp()
		{
			BuildQuad(farCorner, -rightDir, -forwardDir);
		}

		protected void BuildFaceBack()
		{
			BuildQuad(farCorner, -upDir, -rightDir);
		}

		protected void BuildFaceRight()
		{
			BuildQuad(farCorner, -forwardDir, -upDir);
		}
		#endregion

		#region Build Cube
		virtual public void Build()
		{
			BuildFaceDown();
			BuildFaceFront();
			BuildFaceLeft();
			BuildFaceUp();
			BuildFaceBack();
			BuildFaceRight();
		}

		virtual public void BuildNoBottom()
		{
			BuildFaceFront();
			BuildFaceLeft();
			BuildFaceUp();
			BuildFaceBack();
			BuildFaceRight();
		}

		protected void BuildSides()
		{
			BuildFaceFront();
			BuildFaceLeft();
			BuildFaceBack();
			BuildFaceRight();
		}
		#endregion

	}

	#region Specialized Cube Shapes:

	#region House
	class HouseBuilder : CubeBuilder
	{
		public Vector3 roofPeak;
		public Vector3 wallTopLeft;
		public Vector3 wallTopRight;
		public Vector3 dirFromPeakLeft;
		public Vector3 dirFromPeakRight;
		public float overhang;

		public HouseBuilder(MeshData meshData, Vector3 position, float height, float width, float length, float roofHeight, float roofOverhang)
			: base(meshData, Vector3.one, height, width, length, Pivot.CenterBottom)
		{
			overhang = roofOverhang;
			roofPeak = Vector3.up * (height + roofHeight) + rightDir * 0.5f - pivotOffset;

			wallTopLeft = upDir - pivotOffset;
			wallTopRight = upDir + rightDir - pivotOffset;

			dirFromPeakLeft = wallTopLeft - roofPeak;
			dirFromPeakRight = wallTopRight - roofPeak;

			dirFromPeakLeft += dirFromPeakLeft.normalized * overhang;
			dirFromPeakRight += dirFromPeakRight.normalized * overhang;
		}

		protected void BuildHouseRoof()
		{
			BuildTriangle(
				wallTopLeft,
				roofPeak,
				wallTopRight
			);
			BuildTriangle(
				wallTopLeft + forwardDir,
				wallTopRight + forwardDir,
				roofPeak + forwardDir
			);

			roofPeak -= Vector3.forward * overhang;
			forwardDir += Vector3.forward * overhang * 2.0f;

			BuildDoubleSidedQuad(roofPeak, forwardDir, dirFromPeakLeft);
			BuildDoubleSidedQuad(roofPeak, forwardDir, dirFromPeakRight);
		}

		public override void Build()
		{
			BuildSides();
			BuildHouseRoof();
		}
	}

	class FenceBuilder : CubeBuilder
	{
		int postCount;
		Vector3 startPosition;
		Vector3 direction;
		float postDistance;


		public FenceBuilder(MeshData meshData, Vector3 position, float height, float width, int postCount, int postDistance)
			: base(meshData, position, height, width, width, Pivot.CenterBottom)
		{
			this.postCount = postCount;
			this.postDistance = postDistance;


		}

		public override void Build()
		{
			//only right for now
			direction = Vector3.right;

			for (int i = 0; i < postCount; i++)
			{
				Vector3 postPosition = direction * postDistance * i;
				BuildNoBottom();

			}
		}
	}




	#endregion

	#endregion

	#endregion

	#endregion
}